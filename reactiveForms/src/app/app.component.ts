import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';




@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: [ './app.component.css' ]
})
export class AppComponent implements OnInit  {
  // validate;

  loginForm!: FormGroup;
  validate!: boolean;
  

  ngOnInit() {
    this.buildForm();
  }





  buildForm() {
    this.loginForm = new FormGroup({
      username: new FormControl('', [Validators.required]),
      password: new FormControl('')
    })
  }

  submit() {
    this.validate = true;
    if (this.loginForm.valid) {
      const data = this.loginForm.value;
      console.log(data);
    }
  }
}
