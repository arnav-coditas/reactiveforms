import { Component, forwardRef, Input, ViewChild, ElementRef } from '@angular/core';
import {ControlValueAccessor} from '@angular/forms';
import { FormGroup, FormBuilder, FormArray, NG_VALUE_ACCESSOR } from '@angular/forms';

@Component({
  selector: 'app-custom-input',
  templateUrl: './custom-input.component.html',
  styleUrls: ['./custom-input.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => CustomInputComponent),
      multi: true
    }
  ]
})
export class CustomInputComponent implements ControlValueAccessor{
 
  value = '';

  onChange: ((value?: any) => void) | undefined;

  onTouch: ((event: any) => void) | undefined;

  @Input() placeholder = '';

  @Input() autoFocus = false;

  @Input() isDisabled: boolean | undefined;

  @Input() inputType: boolean | undefined;

  @Input() edit: boolean | undefined;

 

  @ViewChild('input', {static: false}) input: ElementRef | undefined;

  ngAfterViewInit() {
    if(this.autoFocus) {
      this.onFocus()
    }
  }

  writeValue(value: any) {
    this.value = value;
  }

  registerOnChange(fn: any): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: any): void {
    this.onTouch = fn;
  }

  setDisableState(status: boolean) {
    this.isDisabled = status;
  }

  onInput(value: any) {
    if(this.onChange) {
      this.onChange(value);
    }
  }

  onTouched(value: any) {
    if(this.onTouch) {
      this.onTouch(value)
    }
  }

  onFocus() {
    //Normal Focus Method
    // this.input.nativeElement.focus();

    // Another Method for set Focus
    //  this.input.nativeElement.select();
  }
}

 

 


